import ballerina/http;
import 'service.datastore as Datastore;
import 'service.types as Types;

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

service /student_api/v1 on ep0 {
    
    //gets all the students
    resource function get students() returns Types:Student[]|http:Response {
        return Datastore:StudentTable.toArray();
    }

    //add students to the table
    resource function post students(@http:Payload Types:Student payload) returns Types:CreatedInlineResponse201|http:Response {
        int[] conflictingStudentNumbers = from var {studentNumber} in Datastore:StudentTable
            where studentNumber == payload.studentNumber
            select studentNumber;
        if conflictingStudentNumbers.length() > 0 {
            http:Response response = new ();
            response.statusCode = 409;
            response.setPayload("Student already exists");
            return response;
        }else{
            Datastore:StudentTable.add(<Types:Student>payload);
            Types:InlineResponse201 inlineresponse = {
                studentid: payload.studentNumber
            };
            Types:CreatedInlineResponse201 response = {
                body: {...inlineresponse}
            };
            return response;
        }
    }

    //get a single student
    resource function get students/[int studentNumber]() returns Types:Student|http:Response {
        Types:Student? student = Datastore:StudentTable.get(studentNumber);
        if student != (){
            return student;
        }else {
            http:Response response = new ();
            response.statusCode = 404;
            response.setPayload("Student not found");
            return response;
        }
    }

    //update student details
    resource function put students/[int studentNumber](@http:Payload Types:Student payload) returns Types:Student|http:Response {
        Types:Student? student = Datastore:StudentTable.get(studentNumber);
        if student != (){
            string? fullname = payload.fullname;
            if fullname != () {
                student.fullname = fullname;
            }

            string? emailaddress = payload.emailaddress;
            if emailaddress != () {
                student.emailaddress = emailaddress;
            }

            return student;
        }else {
            http:Response response = new ();
            response.statusCode = 404;
            response.setPayload("Student not found");
            return response;
        }
    }

    //delete a student
    resource function delete students/[int studentNumber]() returns http:NoContent|http:Response {
        Types:Student? student = Datastore:StudentTable.get(studentNumber);
        if student != (){
            Types:Student remove = Datastore:StudentTable.remove(student.studentNumber);
            return<http:NoContent>{};
        }else {
            http:Response response = new ();
            response.statusCode = 404;
            response.setPayload("Student not found");
            return response;
        }
    }

    //update course details
    resource function put students/[int studentNumber]/course/[string courseCode](@http:Payload Types:Course payload) returns Types:CreatedStudent|http:Response {
        Types:Student? student = Datastore:StudentTable.get(studentNumber);
        if student == () {
            http:Response response = new ();
            response.statusCode = 404;
            response.setPayload("Student not found");
            return response;
        }else {
            Types:Course[] courses = <Types:Course[]>student.courses;
            Types:Course? studentCourse = courses.filter(
                function (Types:Course course) returns boolean {
                    return course.courseCode == courseCode;
                }
            )[0];
            
            if studentCourse == () {
                http:Response response = new ();
                response.statusCode = 404;
                response.setPayload("Course not found");
                return response;
                
            }else {
                Types:CourseAssessments[] assessments = <Types:CourseAssessments[]>payload.assessments;
                if assessments.length() > 0 {
                    Types:CourseAssessments[] studentAssessments = <Types:CourseAssessments[]>studentCourse.assessments;
                    studentAssessments.push(...assessments);
                    studentCourse.assessments = studentAssessments;
                    Types:CreatedStudent response = {
                        body: {...student}
                    };
                    return response;
                }else {
                    http:Response response = new ();
                    response.statusCode = 400;
                    response.setPayload("Assessments not found");
                    return response;
                }
            }
        }

    }
}
