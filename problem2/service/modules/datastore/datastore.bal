import 'service.types as Types;

public table<Types:Statistic> key(region) statisticsTable = table [
        {region: "Khomas", date: "2021-09-20", deaths: 10, confirmed_cases: 100, recoveries: 20, tested: 100},
        {region: "Erongo", date: "2021-09-20", deaths: 10, confirmed_cases: 100, recoveries: 20, tested: 100},
        {region: "Oshana", date: "2021-09-20", deaths: 10, confirmed_cases: 100, recoveries: 20, tested: 100}
    ];
