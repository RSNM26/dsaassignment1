import ballerina/http;

public type CreatedInlineResponse201 record {|
    *http:Created;
    InlineResponse201 body;
|};

public type CreatedStudent record {|
    *http:Created;
    Student body;
|};

public type InlineResponse201 record {
    # the student number of the newly created student
    int studentid?;
};

public type Error record {
    string errorType?;
    string message?;
};

public type CourseAssessments record {
    decimal weight?;
    decimal marks?;
};

public type Student record {
    # the student number of the student
    readonly int studentNumber;
    # the full name of the student
    string fullname?;
    # the email address of the student
    string emailaddress?;
    # the courses of the student
    Course[] courses?;
};

public type Course record {
    # the course code of the course
    string courseCode?;
    CourseAssessments[] assessments?;
};
