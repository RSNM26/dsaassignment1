public type Statistic record {|
    string date;
    readonly string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
|};

public type Error record {|
    string errorType;
    string message;
|};

// public service class Statistic {
    
//     private string date;
//     private string region;
//     private int deaths;
//     private int confirmed_cases;
//     private int recoveries;
//     private int tested;

//     function init(string region) {
//         self.region = region;
//         self.date = "";
//         self.deaths = 0;
//         self.confirmed_cases = 0;
//         self.recoveries = 0;
//         self.tested = 0;
//     }

//     resource function get date() returns string {
//         return self.date;
//     }
//     resource function get region() returns string {
//         return self.region;
//     }
//     resource function get deaths() returns int {
//         return self.deaths;
//     }
//     resource function get confirmed_cases() returns int {
//         return self.confirmed_cases;
//     }
//     resource function get recoveries() returns int {
//         return self.recoveries;
//     }
//     resource function get tested() returns int {
//         return self.tested;
//     }

//     public function updateStatistic(string date, int deaths, int confirmed_cases, int recoveries, int tested) {
//         self.date = date;
//         self.deaths = deaths;
//         self.confirmed_cases = confirmed_cases;
//         self.recoveries = recoveries;
//         self.tested = tested;
//     }

// }