import 'service.types as Types;

public table<Types:Student> key(studentNumber) StudentTable = table[
    {studentNumber: 111111111, fullname: "Rejoice Munwela", emailaddress: "rejoicem@gmail.com", courses: [
        {courseCode: "DSA621S", assessments: [{
            weight: 0.3, marks: 80.0
        }]}
    ]}
];