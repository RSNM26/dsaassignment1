import ballerina/grpc;
import ballerina/io;

assessmentmanagementClient ep = check new ("http://localhost:9090");

public function main() returns error? {

    io:println("1. Create A Course");
    io:println("2. Assign A Course");
    io:println("3. Submit Marks");
    io:println("4. Request Assignments");
    io:println("5. Register for a Course");
    io:println("6. Submit Assignment");
    io:println("7. Create a User");
    string choice = io:readln("Enter a choice to perform an action: ");

    if choice == "1" {
        createCourses();
    } else if choice == "2" {
        assignCourses();
    } else if choice == "3" {
        submitMarks();
    } else if choice == "4" {
        requestAssignments();
    } else if choice == "5" {
        register();
    } else if choice == "6" {
        submitAssignments();
    } else if choice == "7" {
        createUsers();
    } else {
        io:println("Invalid Choice");
    }
}

//completed
function createCourses() {
    do {
        Create_coursesStreamingClient|grpc:Error createCoursesStreamingClient = check ep->create_courses();
        grpc:Error? error1;
        grpc:Error? error2;

        Course[] create_course = [

            {
                courseCode: "SPS2222S",
                assignments: [
                    {assignmentCode: "ASSIGN2", weight: 0.3}
                ]
            }
        ];

        if (createCoursesStreamingClient is error) {

        } else {
            foreach var item in create_course {
                error1 = createCoursesStreamingClient->sendCourse(item);
            }
            error2 = createCoursesStreamingClient->complete();

            string|grpc:Error? response = createCoursesStreamingClient->receiveString();
            io:println(error1);
            io:println(error2);
            io:println(response);
        }

    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }

}

//completed
function assignCourses() {
    do {
        Assign_coursesStreamingClient|grpc:Error assignCoursesStreamingClient = check ep->assign_courses();
        grpc:Error? error1;
        grpc:Error? error2;
        CourseAssessor[] assign_course = [
            {
                courseCode: "DSA621S",
                assessor: {
                    userId: "DANE123",
                    firstname: "Dane",
                    lastname: "Doe",
                    email: "danedoe@gmail.com",
                    profile: "ASSESSOR"
                }
            },
            {
                courseCode: "PRG621S",
                assessor: {
                    userId: "DANE123",
                    firstname: "Dane",
                    lastname: "Doe",
                    email: "danedoe@gmail.com",
                    profile: "ASSESSOR"
                }
            }
        ];

        if (assignCoursesStreamingClient is error) {

        } else {
            foreach var item in assign_course {
                error1 = assignCoursesStreamingClient->sendCourseAssessor(item);
            }
            error2 = assignCoursesStreamingClient->complete();
            string|grpc:Error? response = assignCoursesStreamingClient->receiveString();
            io:println(error1);
            io:println(error2);
            io:println(response);
        }

    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }

}

//completed
function createUsers() {
    do {
        Create_usersStreamingClient|grpc:Error createUsersStreamingClient = check ep->create_users();
        grpc:Error? error1;
        grpc:Error? error2;
        User[] users = [
            {
                userId: "REJOICE123",
                firstname: "Rejoice",
                lastname: "Munwela",
                email: "rejoicem@gmail.com",
                profile: "ASSESSOR"
            }

        ];

        if (createUsersStreamingClient is error) {

        } else {
            foreach var item in users {
                error1 = createUsersStreamingClient->sendUser(item);
            }
            error2 = createUsersStreamingClient->complete();
            string|grpc:Error? response = createUsersStreamingClient->receiveString();
            io:println(error1);
            io:println(error2);
            io:println(response);
        }
    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }

}

//completed
function submitMarks() {
    do {
        Submit_marksStreamingClient|grpc:Error submitMarksStreamingClient = check ep->submit_marks();
        grpc:Error? error1;
        grpc:Error? error2;
        Mark[] marks = [
            {
                userId: "REJOICE123",
                course: {
                    courseCode: "DSA621S",
                    assignments: [
                        {assignmentCode: "ASS1", weight: 0.3}
                    ]
                },
                mark: 80
            }
        ];

        if (submitMarksStreamingClient is error) {

        } else {
            foreach var item in marks {
                error1 = submitMarksStreamingClient->sendMark(item);
            }

            error2 = submitMarksStreamingClient->complete();

            string|grpc:Error? response = submitMarksStreamingClient->receiveString();
            io:println(error1);
            io:println(error2);
            io:println(response);
        }

    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }

}

//completed
function requestAssignments() {
    do {
        stream<Assignment, grpc:Error?> result = check ep->request_assignments("DSA621S");
        check result.forEach(function(Assignment assignment) {
            io:println(assignment);
        });
    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }

}

//completed
function register() {
    do {
        RegisterStreamingClient|grpc:Error registerStreamingClient = check ep->register();
        grpc:Error? error1;
        grpc:Error? error2;
        RegisteredCourse[] registeredCourses = [
            {
                userId: "REJOICE123",
                courses: [
                    {
                        courseCode: "DSA621S",
                        assignments: [
                            {assignmentCode: "ASS1", weight: 0.3}
                        ]
                    }
                ]
            }
        ];

        if (registerStreamingClient is error) {
            
        }else{

        foreach var item in registeredCourses {
            error1 = registerStreamingClient->sendRegisteredCourse(item);
        }
        error2 = registerStreamingClient->complete();
        string|grpc:Error? response = registerStreamingClient->receiveString();

        io:println(error1);
        io:println(error2);
        io:println(response);
        }
    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }
}

//completed
function submitAssignments() {
    do {
        Submit_assignmentsStreamingClient|grpc:Error submitAssignmentsStreamingClient = check ep->submit_assignments();
        grpc:Error? error1;
        grpc:Error? error2;
        SubmitedAssignment[] submittedAssignments = [
            {
                userId: "REJOICE123",
                course: {
                    courseCode: "DSA621S",
                    assignments: [
                        {assignmentCode: "ASS1", weight: 0.0}
                    ]
                },
                marked: false,
                content: "grpc code"
            }
        ];

        if (submitAssignmentsStreamingClient is error) {

        } else {
            foreach var item in submittedAssignments {
                error1 = submitAssignmentsStreamingClient->sendSubmitedAssignment(item);
            }
            error2 = submitAssignmentsStreamingClient->complete();
            string|grpc:Error? response = submitAssignmentsStreamingClient->receiveString();
            io:println(error1);
            io:println(error2);
            io:println(response);
        }
    } on fail var e {
        io:println(`Error: ${e.message()}`);
    }

}
