import ballerina/grpc;
import ballerina/log;
import ballerina/io;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_VLE, descMap: getDescriptorMapVle()}
service "assessmentmanagement" on ep {

    //creating datastores(tables to store data)
    private table<User> key(userId) users;
    private table<Course> key(courseCode) courses;
    private table<Mark> key(userId) marks;
    private table<SubmitedAssignment> key(userId) submitedAssignments;
    private table<CourseAssessor> key(courseCode) courseAssessors;
    private table<RegisteredCourse> key(userId) registeredCourses;

    //adding filler data
    function init() {
        self.users = table [
                {userId: "JOHN123", firstname: "John", lastname: "Doe", email: "johndoe@gmail.com", profile: "LEARNER"},
                {userId: "JANE123", firstname: "Jane", lastname: "Doe", email: "janedoe@gmail.com", profile: "ADMINISTRATOR"},
                {userId: "DANE123", firstname: "Dane", lastname: "Doe", email: "danedoe@gmail.com", profile: "ASSESSOR"}
                
            ];
        self.courses = table [
            {courseCode: "DSA621S", assignments: [
                {assignmentCode: "ASS1", weight: 0.3},
                {assignmentCode: "TEST1", weight: 0.4},
                {assignmentCode: "ASS2", weight: 0.3}
            ]},
            {courseCode: "PRG621S", assignments: [
                {assignmentCode: "INCLASS1", weight: 0.2},
                {assignmentCode: "ASS1", weight: 0.3},
                {assignmentCode: "TEST1", weight: 0.3},
                {assignmentCode: "LAB1", weight: 0.2}
            ]}
        ];
        self.marks = table [
            {
                userId: "",
                course: {
                    courseCode: "DSA621S",
                    assignments: [
                        {assignmentCode: "ASS1", weight: 0.3},
                        {assignmentCode: "TEST1", weight: 0.4},
                        {assignmentCode: "ASS2", weight: 0.3}
                    ]
                },
                mark: 90
            }
        ];
        self.submitedAssignments = table [
            {course: {
                courseCode: "DSA621S",
                assignments: [{assignmentCode: "ASS1", weight: 0.3}]
            }, userId: "JOHN123", content: "gRPC Code", marked: false}
        ];
        self.courseAssessors = table [
            
        ];
        self.registeredCourses = table [
            
        ];
    }

    //assigning courses
    remote function assign_courses(stream<CourseAssessor, grpc:Error?> clientStream) returns string|error {
        log:printInfo("Client Connection Successful");
        string result = "";

        //looping through client input
        check clientStream.forEach(function (CourseAssessor courseAssessor) {
            //saving the data
            self.courseAssessors.add(courseAssessor);
            result = "Course assigned successfully";
        });

        //returning output
        return result;
    }

    //creating users
    remote function create_users(stream<User, grpc:Error?> clientStream) returns string|error {
        
        //looping through client input
        _ = check clientStream.forEach(function (User user) {
            //saving the data
            self.users.add(user);
        });

        //returning output
        return string `Users created successfully`;
    }

    //submitting assignments
    remote function submit_assignments(stream<SubmitedAssignment, grpc:Error?> clientStream) returns string|error {
        //looping through client input
        _ = check clientStream.forEach(function (SubmitedAssignment submitedAssignment) {
            //saving the data
            self.submitedAssignments.add(submitedAssignment);
        });

        //returing output
        return string `Assignments submitted successfully`;
    }

    //submitting marks for the assignments
    remote function submit_marks(stream<Mark, grpc:Error?> clientStream) returns string|error {
        log:printInfo("Client Connection Successful");
        string result = "";

        //looping through client data
        check clientStream.forEach(function (Mark mark) {
            //saving the data
            self.marks.add(mark);
            result = "Marks submitted successfully";
        });

        //returning output
        return result;
    }

    //getting all the unmarked assignments
    remote function request_assignments(string value) returns stream<Assignment, error?>|error {
        log:printInfo("Client Connection Successful");
        Assignment[] assignments = [];

        //retrieving the assessor's courses
        foreach SubmitedAssignment submittedAssignment in self.submitedAssignments {
            if(submittedAssignment.course.courseCode == value){

                //retrieving all unmarked assigments
                if submittedAssignment.marked == false {
                    foreach Course course in self.courses {
                        if course.courseCode == submittedAssignment.course.courseCode {
                            foreach Assignment assignment in course.assignments {
                                if assignment.assignmentCode == submittedAssignment.course.assignments[0].assignmentCode {
                                    assignments.push(assignment);
                                }
                            }
                        }
                    }

                    return assignments.toStream();
                }else {
                    return error grpc:Error("No assignments to mark");
                }
            }else {
                return error grpc:Error("No assignments to mark");
            }
        }
        
        //returing the output
        return assignments.toStream();
    }

    //creating courses
    remote function create_courses(stream<Course, grpc:Error?> clientStream) returns stream<string, error?>|error {
        log:printInfo("Client Connection Successful");
        string[] courseCodes = [];
        check clientStream.forEach(function (Course course) {
            //saving the data
            self.courses.add(course);
            io:println(`Course created successfully: ${course.courseCode}`);
            courseCodes.push(course.courseCode);
        });

        //returning the output
        return courseCodes.toStream();
    }

    //registering for the course
    remote function register(stream<RegisteredCourse, grpc:Error?> clientStream) returns stream<string, error?>|error {
        log:printInfo("Client Connection Successful");
        string[] userIds = [];

        //looping through the client data
        check clientStream.forEach(function (RegisteredCourse registeredCourse) {
            //saving the data
            self.registeredCourses.add(registeredCourse);
            userIds.push(registeredCourse.userId);
        });

        //returning the output
        return userIds.toStream();
    }
}

