import ballerina/graphql;
import 'service.types as Types;
import 'service.datastore as Datastore;

isolated service /graphql on new graphql:Listener(9090) {

    //get all statistics
    resource function get statistic() returns Types:Statistic[] {
       return Datastore:statisticsTable.toArray();
    }
    //get a single statistics
    resource function get singleStat(string region) returns Types:Statistic {
       return Datastore:statisticsTable.get(region);
    }

    //add a statistic
    remote function add(string region, string date, int deaths, int confirmed_cases, int recoveries, int tested) returns Types:Statistic{
        Types:Statistic statistic = {region: region, date: date, deaths: deaths, confirmed_cases: confirmed_cases, recoveries: recoveries, tested:tested};
        Datastore:statisticsTable.add(statistic);
        return statistic;
    }
    
    //update a statistic
    remote function updateStatistic(string region, string? date, int? deaths, int? confirmed_cases, int? recoveries, int? tested) returns Types:Statistic|error {
        Types:Statistic? statistic = Datastore:statisticsTable.get(region);
        if statistic != (){
            if date != () {
                statistic.date = date;
            }
            if deaths != () {
                statistic.deaths = deaths;
            }
            if confirmed_cases != () {
                statistic.confirmed_cases = confirmed_cases;
            }
            if recoveries != () {
                statistic.recoveries = recoveries;
            }
            if tested != () {
                statistic.tested = tested;
            }
            return statistic;
        } else {

            return error("Key not found in the table");
        }

    }
}

